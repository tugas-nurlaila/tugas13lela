<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DosenController extends Controller
{
    public function formulir()
    {
        return view('formulir');
    }

    public function kirim(Request $request)
    {
        $nama = $request["namaa"];
        return view('selamat', ['nama' => $nama]);
    }
}
